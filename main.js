import GeoJSON from 'ol/format/GeoJSON';
import Map from 'ol/Map';
import MultiPoint from 'ol/geom/MultiPoint';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import View from 'ol/View';
import {Circle as CircleStyle, Fill, Stroke, Style} from 'ol/style';
import Point from 'ol/geom/Point';
import Text from 'ol/style/Text';

function styles(feature) {
  let multipoint = new MultiPoint(feature.getGeometry().getCoordinates()[0])

  let styles = [
    new Style({
      stroke: new Stroke({
        color: 'pink',
        width: 5
      }),
      fill: new Fill({
        color: 'rgba(0, 0, 255, 0.1)'
      })
    })
  ]

  multipoint.getCoordinates().forEach(function(coordinates, index, arr) {
    styles.push(new Style({
      zIndex: 10,
      image: new CircleStyle({
        radius: 15,
        fill: new Fill({
          color: `rgba(255, 0, 0, 0.7)`
        })
      }),
      text: new Text({
        text: index < arr.length-1 ? index.toString() : '',
        font: '20px system-ui',
        fill: new Fill({
          color: 'white'
        })
      }),
      geometry: new Point(coordinates)
    }))
  })
  return styles
}

const geojsonObject = {
  'type': 'FeatureCollection',
  'crs': {
    'type': 'name',
    'properties': {
      'name': 'EPSG:3857',
    },
  },
  'features': [
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Polygon',
        'coordinates': [
          [
            [-5e6, 6e6],
            [-5e6, 8e6],
            [-3e6, 8e6],
            [-3e6, 6e6],
            [-5e6, 6e6],
          ],
        ],
      },
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Polygon',
        'coordinates': [
          [
            [-2e6, 6e6],
            [-2e6, 8e6],
            [0, 8e6],
            [0, 6e6],
            [-2e6, 6e6],
          ],
        ],
      },
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Polygon',
        'coordinates': [
          [
            [1e6, 6e6],
            [1e6, 8e6],
            [3e6, 8e6],
            [3e6, 6e6],
            [1e6, 6e6],
          ],
        ],
      },
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Polygon',
        'coordinates': [
          [
            [-2e6, -1e6],
            [-1e6, 1e6],
            [0, -1e6],
            [-2e6, -1e6],
          ],
        ],
      },
    },
  ],
};

const source = new VectorSource({
  features: new GeoJSON().readFeatures(geojsonObject),
});

const layer = new VectorLayer({
  source: source,
  style: styles,
});

const map = new Map({
  layers: [layer],
  target: 'map',
  view: new View({
    center: [0, 3000000],
    zoom: 2,
  }),
});